package moduleLib;

import genericLib.WebDriverInterActions;

public class PlanYourTrip {
	
WebDriverInterActions webDriverInterActions;
	
	public PlanYourTrip(WebDriverInterActions webDriverInterActions){
		this.webDriverInterActions=webDriverInterActions;
	}
	
	public void planYourTripM(String From,String To,String Day,String Hour,String Minute){
		webDriverInterActions.SendKeys("PlanYourTrip.from", From);
		webDriverInterActions.SendKeys("PlanYourTrip.to", To);
		webDriverInterActions.click("PlanYourTrip.arriveBfr");
		webDriverInterActions.selectbytext("PlanYourTrip.day",Day);
		webDriverInterActions.selectbytext("PlanYourTrip.hour",Hour);
		webDriverInterActions.selectbytext("PlanYourTrip.minute",Minute);
		webDriverInterActions.click("PlanYourTrip.remember");
		/*  Add Assertion*/
		webDriverInterActions.assertTrueMethod("PlanYourTrip.viewTrip");
		webDriverInterActions.click("PlanYourTrip.viewTrip");

}

}
