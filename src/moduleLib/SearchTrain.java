package moduleLib;

import genericLib.WebDriverInterActions;

public class SearchTrain {
	WebDriverInterActions webDriverInterActions;
	
	public SearchTrain(WebDriverInterActions webDriverInterActions){
		this.webDriverInterActions=webDriverInterActions;
	}
	
	public void searchTrainM(String Line,String Direction,String Day,String Hour,String Minute){
		webDriverInterActions.selectbytext("HomepageConstant.line", Line);
		webDriverInterActions.selectbytext("HomepageConstant.direction", Direction);
		webDriverInterActions.click("HomepageConstant.arrBfr");
		webDriverInterActions.selectbytext("HomepageConstant.day",Day);
		webDriverInterActions.selectbytext("HomepageConstant.hour",Hour);
		webDriverInterActions.selectbytext("HomepageConstant.minute",Minute);
		webDriverInterActions.click("HomepageConstant.rememberMe");
		/*  Add Assertion*/
		webDriverInterActions.assertTrueMethod("HomepageConstant.viewTable");
		webDriverInterActions.click("HomepageConstant.viewTable");

}
}