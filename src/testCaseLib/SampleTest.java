package testCaseLib;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import genericLib.WebDriverInterActions;
import moduleLib.SampleProgram;

public class SampleTest {
	WebDriverInterActions webDriverIntractions;
	SampleProgram getCarQuote;
	

	@BeforeMethod
	public void beforemethod() {		
		/* Open Browser */
		
		webDriverIntractions =new  WebDriverInterActions();
		webDriverIntractions.navigateURL();
	    getCarQuote=new SampleProgram(webDriverIntractions);
		
}

	
  @Test
  public void sampleTest() throws Exception {
	  
	  getCarQuote.getCarQuoteMethod("Audi","2018","20","New South Wales","praveer.silare@gmail.com");
	  webDriverIntractions.takeSnapShot("/ScreenShot/screenshot.png");
			  
}
  
//  @AfterMethod
//  public void afterMethod() {		
//		/* Close Browser */
//	  
//	  webDriverIntractions.closebrowser();	
//		
//}

  
}
