package testCaseLib;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import genericLib.WebDriverInterActions;
import moduleLib.PlanYourTrip;


public class PlanYourTripTest {
	
	/* TestCae2:
	 * open browser
	 * navigate to URl
	 *  Plan your Trip table
	 *  select Origin
	 *  select Destination
	 *  select day 
	 *  and so on
	 *  click on viewTrip*/
	WebDriverInterActions webDriverIntractions;
	PlanYourTrip planYourTrip;
	

	@BeforeMethod
	public void beforemethod() {		
		/* Open Browser */
		
		webDriverIntractions =new  WebDriverInterActions();
		/*Navigate to URL : www.SydneyTrain
		 */
		webDriverIntractions.navigateURL();
		
		planYourTrip=new PlanYourTrip(webDriverIntractions); 
		
}
  @Test
  public void planYourTripMethod() throws Exception {
	  /*Plan your trip Method
		 */
	  planYourTrip.planYourTripM("Kogarah Station, Kogarah","Kiama Station, Kiama", "7-Feb-2018 (Wed)", "10", "10");
	  webDriverIntractions.waitforPageToLoad();
	  /*take ScreenShot 
		 */
	  webDriverIntractions.takeSnapShot("/ScreenShot/planYourTrip.png");
	  
	  
  }
  
@AfterMethod
public void afterMethod() {		
		/* Close Browser */
	  
	  webDriverIntractions.closebrowser();	
		
}


}
