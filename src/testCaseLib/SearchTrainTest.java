package testCaseLib;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import genericLib.WebDriverInterActions;
import moduleLib.SampleProgram;
import moduleLib.SearchTrain;

public class SearchTrainTest {
	/* TestCae1:
	 * open browser
	 * navigate to URl
	 *  Search train time table table
	 *  select line
	 *  select direction
	 *  select day 
	 *  and so on
	 *  click on viewTables*/
	
	
	WebDriverInterActions webDriverIntractions;
	SearchTrain searchtrain;
	

	@BeforeMethod
	public void beforemethod() {		
		/* Open Browser */
		
		webDriverIntractions =new  WebDriverInterActions();
		/*Navigate to URL : www.SydneyTrain
		 */
		webDriverIntractions.navigateURL();
		searchtrain=new SearchTrain(webDriverIntractions); 
		
}
  @Test
  public void searchTrainMethod() throws Exception {
	  /*Search for train time table
		 */
	  searchtrain.searchTrainM("Hunter Line", "Scone or Dungog to Newcastle Interchange", "7-Feb-2018 (Wed)", "10", "10");
	  webDriverIntractions.waitforPageToLoad();
	  /*take ScreenShot 
		 */
	  webDriverIntractions.takeSnapShot("/ScreenShot/searchtrain.png");
	  
	  
  }
  
@AfterMethod
public void afterMethod() {		
		/* Close Browser */
	  
	  webDriverIntractions.closebrowser();	
		
}


}
