package genericLib;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
	 
	public static WebDriver driver;
	public static WebDriver getdesiredbrowser() {
		
//	browser=ReadPropertiesutil.getdesiredproperty(APPCONSTANTS.BROWSER);
	
		switch(ReadByFileUtility.getdesiredproperty(APPCONSTANT.BROWSER).toUpperCase()){
				
		case "FIREFOX":
					driver=getFirefoxdriver();
					break;
		case "CHROME":
					driver=getchomredriver();
					break;				
			}
			return driver;
			
		}
		public static WebDriver getchomredriver() {	
			System.setProperty("webdriver.driver.chrome",System.getProperty("user.dir")+"//Drivers//chromedriver.exe");
			driver=new ChromeDriver();
			return driver;
		
		
		}
		
		public static WebDriver getFirefoxdriver() {	
			System.setProperty("webdriver.driver.gekodriver",System.getProperty("user.dir")+"//Drivers//gekodriver.exe");
			driver=new FirefoxDriver();
			return driver;
		
		
		}
		



}
