package genericLib;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadByFileUtility {
	static Properties pro;
	 
	public static Properties loadfiles() {
		try {
			if(pro==null) {
				
				pro=new Properties();
				File f=new File(System.getProperty("user.dir")+"/Drivers/");
				File[] allfiles=f.listFiles();
				for( int i=0;i<f.listFiles().length;i++) {
				 pro.load(new FileInputStream(f.listFiles()[i]));				 
				}
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return pro;
		
	}
	public static String getdesiredproperty(String keyvalue) {
		pro=loadfiles();
		return pro.getProperty(keyvalue);
	}



}
