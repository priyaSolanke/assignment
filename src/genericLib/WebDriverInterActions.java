package genericLib;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class WebDriverInterActions {
public WebDriver driver;
	
	public WebDriver getCurrentDriver() {
	if(	driver==null) {

		driver=BrowserFactory.getdesiredbrowser();
	}
		
		return driver;
	}
	
	public WebElement FINDElement(String BykeyValue) {
		waitforPageToLoad();
		return getCurrentDriver().findElement(ReadByObject.getbyproperty(BykeyValue));
		
	}
	public List<WebElement> FINDElements(String BykeyValue) {
		waitforPageToLoad();
		return getCurrentDriver().findElements(ReadByObject.getbyproperty(BykeyValue));
	}
	public void click(String BykeyValue) {
		FINDElement(BykeyValue).click();
		//Assert.assertTrue(BykeyValue.isEnabled());
	}
	public void SendKeys(String BykeyValue,String text) {
		FINDElement(BykeyValue).clear();
		FINDElement(BykeyValue).sendKeys(text);
	}
	public void switchToSecondWIndow() {
		Set<String> windows=getCurrentDriver().getWindowHandles();
		Iterator<String> itr=windows.iterator();
			String firstwindow=itr.next();
			String secondwind=itr.next();
			getCurrentDriver().switchTo().window(secondwind);
	}
	public void switchTofirstWindow() {
		Set<String> windows=getCurrentDriver().getWindowHandles();
		Iterator<String> itr=windows.iterator();
			String firstwindow=itr.next();
			String secondwind=itr.next();
			getCurrentDriver().switchTo().window(firstwindow);
	}
	
	public void selectbytext(String ByKeyValue,String text) {
		Select sel=new Select(FINDElement(ByKeyValue));
		sel.selectByVisibleText(text);
	}
	
	
	public  void navigateURL(){
		getCurrentDriver().get(ReadByFileUtility.getdesiredproperty(APPCONSTANT.URL));
	}
	public void waitforPageToLoad() {
		getCurrentDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 
	}
	public void closebrowser() {
		getCurrentDriver().quit();
	}
	
	 public void takeSnapShot(String FileName) throws Exception{

	        //Convert web driver object to TakeScreenshot
		 EventFiringWebDriver eventFiringWebDriver=new EventFiringWebDriver(getCurrentDriver());
             //Call getScreenshotAs method to create image file
         File SrcFile=eventFiringWebDriver.getScreenshotAs(OutputType.FILE);
             //Move image file to new destination
         File DestFile=new File(System.getProperty("user.dir")+FileName);
              //Copy file at destination
          FileUtils.copyFile(SrcFile, DestFile);

	    }

 public void assertTrueMethod(String BykeyValue){
	 Assert.assertTrue(FINDElement( BykeyValue).isEnabled());
 }

}
